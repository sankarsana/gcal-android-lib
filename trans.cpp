#include <stdlib.h>

#include "platform.h"
#include "avc.h"
#include "apc.h"
#include "ttimezone.h"

Boolean GCalApp_InitInstance();//char * args[], int argn);
int GCalApp_InitFolders();

void GCalApp_InitInstanceData();

const char *GCalApp_GetFileName(int n);


extern int g_dstSelMethod;
extern CLocation gMyLocation;
extern CLocation gLastLocation;
extern VCTIME gToday;
extern VCTIME gTomorrow;
extern VCTIME gYesterday;
extern int g_BhanuMode;
extern TLangFileList gLangList;

Boolean GCalApp_InitLanguageOutputFromFile(const char *pszFile) {
    return false;
}

Boolean GCalApp_GetLangFileForAcr(const char *pszAcr, TString &strFile) {
    return false;
}

Boolean GCalApp_ParseCommandArguments(int argc, char *args[]) {
    return false;

    if (argc < 2)
        return false;

    CLocation loc;
    VCTIME vcStart, vcEnd;
    VATIME vaStart, vaEnd;
    int nCount;
    int nReq;
    TString str;
    TString strFileOut;
    FILE *fout = stdout;

    loc.m_fLatitude = 0.0;
    loc.m_fLongitude = 0.0;
    loc.m_fTimezone = 0.0;
    loc.m_nDST = 0;
    loc.m_strCity = "";
    loc.m_strCountry = "";
    vcStart.day = 0;
    vcStart.month = 0;
    vcStart.year = 0;
    vcEnd = vcStart;
    vaStart.tithi = vaStart.masa = vaStart.gyear = 0;
    vaEnd = vaStart;
    nCount = -1;

    for (int i = 1; i < argc; i++) {
        if (strcmp(args[i], "-L") == 0) {
            if (argc >= i + 2) {
                ApcString_GetArg_EarthPos(args[i + 1], loc.m_fLatitude, loc.m_fLongitude);
            }
            i++;
        } else if (strcmp(args[i], "-N") == 0) {
            if (argc >= i + 2) {
                loc.m_strCity = args[i + 1];
            }
            i++;
        } else if (strcmp(args[i], "-SV") == 0) {
            if (argc >= i + 2) {
                ApcString_GetArg_VaisnDate(args[i + 1], vaStart);
            }
            i++;
        } else if (strcmp(args[i], "-SG") == 0) {
            if (argc >= i + 2) {
                ApcString_GetArg_Date(args[i + 1], vcStart);
            }
            i++;
        } else if (strcmp(args[i], "-ST") == 0) {
            if (argc >= i + 2) {
                ApcString_GetArg_Time(args[i + 1], vcStart);
            }
            i++;
        } else if (strcmp(args[i], "-EG") == 0) {
            if (argc >= i + 2) {
                ApcString_GetArg_Date(args[i + 1], vcEnd);
            }
            i++;
        } else if (strcmp(args[i], "-EV") == 0) {
            if (argc >= i + 2) {
                ApcString_GetArg_VaisnDate(args[i + 1], vaEnd);
            }
            i++;
        } else if (strcmp(args[i], "-EC") == 0) {
            if (argc >= i + 2) {
                nCount = atoi(args[i + 1]);
            }
            i++;
        } else if (strcmp(args[i], "-TZ") == 0) {
            if (argc >= i + 2) {
                ApcString_GetArg_TimeZone(args[i + 1], loc.m_fTimezone);
            }
            i++;
        } else if (strcmp(args[i], "-LNG") == 0) {
            if (argc >= i + 2) {
                TString strLang;
                strLang = args[i + 1];
                TString strFile;
                if (GCalApp_GetLangFileForAcr(strLang, strFile) == true) {
                    GCalApp_InitLanguageOutputFromFile(strFile);
                }
            }
            i++;
        } else if (strcmp(args[i], "-DST") == 0) {
            if (argc >= i + 2) {
                loc.m_nDST = TTimeZone::GetID(args[i + 1]);
            }
            i++;
        } else if (strcmp(args[i], "-O") == 0) {
            if (argc >= i + 2) {
                if (fout != stdout && fout != stderr)
                    fclose(fout);
                fout = fopen(args[i + 1], "wt");
                if (fout == NULL)
                    fout = stderr;
            }
            i++;
        } else if (stricmp(args[i], "-R") == 0) {
            if (argc >= i + 2) {
                if (stricmp(args[i + 1], "calendar") == 0) {
                    nReq = 10;
                } else if (stricmp(args[i + 1], "appday") == 0) {
                    nReq = 11;
                } else if (stricmp(args[i + 1], "tithi") == 0) {
                    nReq = 12;
                } else if (stricmp(args[i + 1], "sankranti") == 0) {
                    nReq = 13;
                } else if (stricmp(args[i + 1], "naksatra") == 0) {
                    nReq = 14;
                } else if (stricmp(args[i + 1], "firstday") == 0) {
                    nReq = 15;
                } else if (stricmp(args[i + 1], "gcalendar") == 0) {
                    nReq = 16;
                } else if (stricmp(args[i + 1], "gtithi") == 0) {
                    nReq = 17;
                } else if (stricmp(args[i + 1], "next") == 0) {
                    nReq = 18;
                } else if (stricmp(args[i + 1], "xlan") == 0) {
                    nReq = 50;
                } else if (stricmp(args[i + 1], "help") == 0) {
                    nReq = 60;
                }
            }
            i++;
        }
    }

    vcStart.tzone = loc.m_fTimezone;
    vcEnd.tzone = loc.m_fTimezone;

    switch (nReq) {
        case 10:
        case 13:
        case 14:
            if (vcStart.year == 0 && vaStart.gyear != 0)
                VATIMEtoVCTIME(vaStart, vcStart, (EARTHDATA) loc);
            if (vcEnd.year == 0 && vaEnd.gyear != 0)
                VATIMEtoVCTIME(vaEnd, vcEnd, (EARTHDATA) loc);
            break;
        default:
            break;
    }

    if (vcStart.year != 0 && vcEnd.year != 0 && nCount < 0)
        nCount = int(vcEnd.GetJulian() - vcStart.GetJulian());

    if (nCount < 0)
        nCount = 30;

    TResultApp appday;
    TResultCalendar calendar;

    switch (nReq) {
        case 10:
            vcStart.NextDay();
            vcStart.PreviousDay();
            CalcCalendar(calendar, loc, vcStart, nCount);
            WriteCalendarXml(calendar, fout);
            break;
        case 11:
            CalcAppDay(loc, vcStart, appday);
            FormatAppDayXML(appday, str);
            fputs(str, fout);
            break;
        case 12:
            WriteXML_Tithi(fout, loc, vcStart);
            break;
        case 13:
            if (vcEnd.year == 0) {
                vcEnd = vcStart;
                vcEnd += nCount;
            }
            WriteXML_Sankrantis(fout, loc, vcStart, vcEnd);
            break;
        case 14:
            WriteXML_Naksatra(fout, loc, vcStart, nCount);
            break;
        case 15:
            WriteXML_FirstDay_Year(fout, loc, vcStart);
            break;
        case 16:
            vcStart = GetFirstDayOfYear((EARTHDATA) loc, vcStart.year);
            vcEnd = GetFirstDayOfYear((EARTHDATA) loc, vcStart.year + 1);
            nCount = int(vcEnd.GetJulian() - vcStart.GetJulian());
            CalcCalendar(calendar, loc, vcStart, nCount);
            WriteCalendarXml(calendar, fout);
            break;
        case 17:
            WriteXML_GaurabdaTithi(fout, loc, vaStart, vaEnd);
            break;
        case 18:
            WriteXML_GaurabdaNextTithi(fout, loc, vcStart, vaStart);
            break;
        case 50: {
            int i = 0;
            TString str;
            fputs("10000\nEnglish\n10001\nen\n", fout);
            // export obsahu do subora
            for (i = 0; i < 900; i++) {
                if (!gstr[i].IsEmpty()) {
                    str.Format("%d\n%s\n", i, gstr[i].c_str());
                    fputs(str, fout);
                }
            }
        }
            break;
    }
    return true;
}

Boolean GCalApp_InitInstance() {
    InitGlobalStrings();
    g_BhanuMode = 0;
    return true;
}